/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */

#include "poisson.h"
#include <math.h>
#include <iostream>
#include <cassert>

#define PI 3.14159265

int main(int argc, char **argv) {
    assert(argc > 1);

    MPI_Init(&argc, &argv);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    double t1, t2;
    t1 = MPI_Wtime();

    if (rank == 0) {
        int size;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        std::cout << "Number of Processors: " << size << std::endl;;
    }

    /** add initialization, get n from command line */

    MPI_Comm grid_comm;
    int n = atoi(argv[1]);
    grid_t x;


    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

    /** fill a and f based on x **/
    vec_t a, f;

    for (size_t i = 0; i < x.size(); ++i) {
        a.push_back(12);
        f.push_back(sin(4 * PI * x[i].x) * sin(10 * PI * x[i].y) * sin(14 * PI * x[i].z) * (12 + 312 * pow(PI, 2)));
    }


    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a),
                            std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)
    vec_t v, rhs, res;
    for (size_t i = 0; i < x.size(); ++i) {
        v.push_back(sin(4 * PI * x[i].x) * sin(10 * PI * x[i].y) * sin(14 * PI * x[i].z));
    }

    real_t res_norm;
    residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);

    t2 = MPI_Wtime();
    if (rank == 0) {
        std::cout << "Residual norm: " << res_norm << std::endl;
        std::cout << "Elapsed time is " << t2 - t1 << std::endl;
    }
    /** cleanup **/
    return 0;
}
