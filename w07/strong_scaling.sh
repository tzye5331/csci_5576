#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH -o strong_possion-%j.out
#SBATCH -e strong_possion-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 27

# run the program
for i in {1,8,27,1,4,9,16,25}; do mpirun -n i ./test_poisson.exe 240; done
