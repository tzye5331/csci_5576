/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <math.h>
#include <iostream>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm, grid_t &x) {
    int nodes;
    int rank;

    MPI_Comm_size(comm, &nodes);
    MPI_Comm_rank(comm, &rank);

    if (rank == 0) {
        std::cout << "setup" << std::endl;
    }

    int dim[DIM] = {};
    MPI_Dims_create(nodes, DIM, dim);

    int periods[DIM] = {};
    int reorder = 0;
    MPI_Cart_create(comm, DIM, dim, periods, reorder, &grid_comm);

    int coords[DIM] = {};
    MPI_Cart_coords(grid_comm, rank, DIM, coords);

    real_t h = 1.0 / (n - 1);

    for (int i = 0; i < n / dim[0]; ++i) {
        for (int j = 0; j < n / dim[1]; ++j) {
            for (int k = 0; k < n / dim[2]; ++k) {
                point_t p;
                p.x = (coords[0] * (n / dim[0]) + i) * h;
                p.y = (coords[1] * (n / dim[1]) + j) * h;
                p.z = (coords[2] * (n / dim[2]) + k) * h;
                x.push_back(p);
            }
        }
    }
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv) {
    int rank;
    MPI_Comm_rank(grid_comm, &rank);

    int size;
    MPI_Comm_size(grid_comm, &size);

    size_t source, dest;
    MPI_Status st;

    if (rank == 0) {
        std::cout << "matvec" << std::endl;
    }

    int dims[DIM] = {};
    MPI_Dims_create(size, DIM, dims);

    for (size_t i = 0; i < DIM; ++i) {
        dims[i] = n / dims[i];
    }

    int coords[DIM] = {};
    MPI_Cart_coords(grid_comm, rank, DIM, coords);

    int index[DIM] = {};

    real_t h = 1.0 / (n - 1);

    int ilim = (coords[0] + 1) * dims[0] < n ? dims[0] : n - (coords[0]) * dims[0];
    int jlim = (coords[1] + 1) * dims[1] < n ? dims[1] : n - (coords[1]) * dims[1];
    int klim = (coords[2] + 1) * dims[2] < n ? dims[2] : n - (coords[2]) * dims[2];

    // Begin MPI communication
    for (size_t i = 0; i < ilim; ++i) {
        for (size_t j = 0; j < jlim; ++j) {
            for (size_t k = 0; k < klim; ++k) {
                double lv_val = 0;

                int index = get_index(i, j, k, ilim, jlim);

                double vval = v[index];
                double aval = a[index];

                if (i == 0) {
                    lv_val += val_mpi_comm(grid_comm, 0, -1, vval);
                    lv_val += v[get_index(i + 1, j, k, ilim, jlim)] - vval;
                } else if (i == ilim - 1) {
                    lv_val += val_mpi_comm(grid_comm, 0, 1, vval);
                    lv_val += v[get_index(i - 1, j, k, ilim, jlim)] - vval;
                } else {
                    lv_val += v[get_index(i + 1, j, k, ilim, jlim)] - vval;
                    lv_val += v[get_index(i - 1, j, k, ilim, jlim)] - vval;
                }

                if (j == 0) {
                    lv_val += val_mpi_comm(grid_comm, 1, -1, vval);
                    lv_val += v[get_index(i, j + 1, k, ilim, jlim)] - vval;
                } else if (j == jlim - 1) {
                    lv_val += val_mpi_comm(grid_comm, 1, 1, vval);
                    lv_val += v[get_index(i, j - 1, k, ilim, jlim)] - vval;
                } else {
                    lv_val += v[get_index(i, j + 1, k, ilim, jlim)] - vval;
                    lv_val += v[get_index(i, j - 1, k, ilim, jlim)] - vval;
                }

                if (k == 0) {
                    lv_val += val_mpi_comm(grid_comm, 2, -1, vval);
                    lv_val += v[get_index(i, j, k + 1, ilim, jlim)] - vval;
                } else if (k == klim - 1) {
                    lv_val += val_mpi_comm(grid_comm, 2, 1, vval);
                    lv_val += v[get_index(i, j, k - 1, ilim, jlim)] - vval;
                } else {
                    lv_val += v[get_index(i, j, k + 1, ilim, jlim)] - vval;
                    lv_val += v[get_index(i, j, k - 1, ilim, jlim)] - vval;
                }

                lv_val = aval * vval - lv_val / (h * h);
                lv[index] = lv_val;
            }
        }
    }

}

void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs,
              vec_t &res, real_t &res_norm) {
    int rank;
    MPI_Comm_rank(comm, &rank);

    if (rank == 0) {
        std::cout << "residual" << std::endl;
    }

    real_t local_res_norm = 0;
    vec_t lv;
    lv.reserve(v.size());

    mv(v, lv);

    for (unsigned int i = 0; i < v.size(); ++i) {
        res.push_back(rhs[i] - lv[i]);
        local_res_norm += pow(res[i], 2);
    }

    std::cout << "Done by processor: " << rank << "\n";

    MPI_Reduce(&local_res_norm, &res_norm, 1, MPI::DOUBLE, MPI_SUM, 0, comm);

    if (rank == 0) {
        res_norm = pow(res_norm, 1 / 2);
    }
}

int get_index(int i, int j, int k, double i_range, double j_range) {
    return k + j * j_range + i * i_range * j_range;
}

double val_mpi_comm(const MPI_Comm &grid_comm, int dim, int shift, double val) {
    int source;
    int dest;
    double recv_val;

    MPI_Status status;

    MPI_Cart_shift(grid_comm, dim, shift, &source, &dest);

    if (dest != MPI_PROC_NULL) {
        MPI_Send(&val, 1, MPI_DOUBLE, dest, 0, grid_comm);
    }

    MPI_Cart_shift(grid_comm, dim, shift * -1, &source, &dest);
    if (dest != MPI_PROC_NULL) {
        MPI_Recv(&recv_val, 1, MPI_DOUBLE, dest, 0, grid_comm, &status);
        return recv_val - val;
    }

    return 0;

}
