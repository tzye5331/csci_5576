## Task 1

### 1 & 2
I completed a compilable executable with `poisson.cc` and `test_poisson.cc`.

However, when I run `strong_scaling.sh` and `weak_scaling.sh`, 
there are still problems that the process crushed at some nodes, 
making an incomplete execution.

### 3, 4, 5
I submitted my best implementation so far (w/o running the analysis for the figures).

