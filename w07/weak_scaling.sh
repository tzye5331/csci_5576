#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH -o weak_possion-%j.out
#SBATCH -e weak_possion-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 64

# run the program
for i in {8,27,64}; do mpirun -n i ./test_poisson.exe $((300*$i)); done
