#include <mpi.h>
#include <iostream>

#include "slurp_file.h"

using vec_int_t = std::vector<int>;
void compute_mat_vec(vec_int_t& v_slot, const vec_int_t& v, const vec_int_t& x) noexcept {
    if (v_slot.size() == v.size()) {
        v_slot.clear();
        for (auto const& v_: v) {
            int ind = &v_ - &v[0];
            v_slot[ind] = v_ * x[ind];
        }
    }
}

void add_vector(vec_int_t& v_f, const vec_int_t& v1, const vec_int_t& v2) noexcept {
    if (v_f.size() == v1.size() == v2.size()) {
        v_f.clear();
        for (auto const& v_: v1) {
            int ind = &v_ - &v1[0];
            v_f[ind] = v1[ind] + v2[ind];
        }
    }
}


int main(int argc, char** argv) noexcept {
    if (argc < 3){
        std::cerr << "Filename and the number of columns are required arguments.";
        return 0;
    }
    char *filename = argv[1];
    size_t num_of_col = atoi(argv[2]);
    int numtasks, taskid;

    MPI_Init(&argc, &argv);
    MPI_Comm_size (MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank (MPI_COMM_WORLD, &taskid);
    MPI_Status st;

    int rank;
    vec_int_t v;
    v.resize(numtasks);
    slurp_file_line(filename, taskid, v);

    vec_int_t x;
    slurp_file_line(filename, numtasks, x);;

    vec_int_t v_to_transmit, v_local, v_temp;
    v_to_transmit.resize(num_of_col);
    v_local.resize(num_of_col);
    v_temp.resize(num_of_col);

    if (taskid == numtasks) {  // rightmost processor
        compute_mat_vec(v_to_transmit, v, x);
        MPI_Send(&v_to_transmit[0], num_of_col, MPI_INT, taskid - 1, 0, MPI_COMM_WORLD);
    }
    
    if (taskid > 0 && taskid < numtasks - 1) {
        MPI_Recv(&v_temp[0], num_of_col, MPI_INT, taskid + 1, 0, MPI_COMM_WORLD, &st);
        compute_mat_vec(v_local, v, x);

        add_vector(v_to_transmit, v_local, v_temp);
        MPI_Send (&v_to_transmit[0], num_of_col, MPI_INT, taskid - 1, 0, MPI_COMM_WORLD);
    }

    if (taskid == numtasks - 1) {
        MPI_Recv(&v_temp[0], num_of_col, MPI_INT, taskid + 1, 0, MPI_COMM_WORLD, &st);
        compute_mat_vec(v_local, v, x);
        add_vector(v_to_transmit, v_local, v_temp);
        std::cout << "Computed final vector is " << "\n";
        for (auto const& v: v_to_transmit) {
            std::cout << v << " ";
        }
        std::cout << "\n (I am done for Task 2-5.)";
        MPI_Finalize();
        return 0;
    }
}
