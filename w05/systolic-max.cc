#include <mpi.h>
#include <iostream>

#include "slurp_file.h"

int get_max(std::vector<data_t>& v) {
    int max = 0;
    for (auto const& v_: v) {
         if (v_ > max) {
             max = v_;
         }
    }
    return max;
}



int main(int argc, char** argv) {
    if (argc <2){
        std::cerr << "Filename is a required argument";
        return 0;
    }
    char *filename = argv[1];
    int numtasks, taskid;

    MPI_Init(&argc, &argv);
    MPI_Comm_size (MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank (MPI_COMM_WORLD, &taskid);
    MPI_Status st;

    int rank;
    std::vector<data_t> v;
    v.resize(numtasks);
    slurp_file_line(filename, taskid, v);


    int slot, global_max;
    if (taskid == numtasks - 1) {
        int right_max = get_max(v);
        MPI_Send(&right_max, 1, MPI_INT, taskid - 1, 0, MPI_COMM_WORLD);  // send data to the left
        std::cout << "Data " << right_max << " sent to its left processor " << taskid - 1 << "...\n";
    }

    if (taskid >= 0 && taskid < numtasks - 1) {
        int to_compare_with_received = get_max(v);

        MPI_Recv(&slot, 1, MPI_INT, taskid + 1, 0, MPI_COMM_WORLD, &st);
        std::cout << "Data " << slot << " was received from processor " << taskid + 1 << "! \n";
        global_max = (slot > to_compare_with_received) ? slot : to_compare_with_received;
        if (taskid != 0) {
            std::cout << "After comparison with the right processor, the MAX in process " << taskid << " is " << global_max << "\n";
            MPI_Send(&global_max, 1, MPI_INT, taskid - 1, 0, MPI_COMM_WORLD);
        } else {
            std::cout << "The global MAX, collected at processor " << taskid << ", is " << global_max << "\n";
        }
    }

    MPI_Finalize();
    return 0;
}
