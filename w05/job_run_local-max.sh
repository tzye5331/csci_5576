#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o local-max-%j.out
#SBATCH -e local-max-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 4

mpirun -n 4 ./local-max.exe array.txt

