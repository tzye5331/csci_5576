#include <mpi.h>
#include <iostream>

int main(){
    MPI_Init(nullptr, nullptr);

    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    std::cout << "Hello from processor " << rank << " of " << size << "\n";


    MPI_Finalize();
    return 0;
}
