#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o hello-mpi-%j.out
#SBATCH -e hello-mpi-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 4

mpirun -n 4 ./mpi_hello_world.exe

