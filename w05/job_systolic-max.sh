#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o systolic-max-%j.out
#SBATCH -e systolic-max-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 4

mpirun -n 4 ./systolic-max.exe array.txt


