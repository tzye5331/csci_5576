#!/bin/bash

#SBATCH --time=0:05:00
#SBATCH -o systolic-matvec-%j.out
#SBATCH -e systolic-matvec-%j.err
#SBATCH --qos debug
#SBATCH --ntasks 4

mpirun -n 4 ./systolic-matvec.exe array.txt 12


