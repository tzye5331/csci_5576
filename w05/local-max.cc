#include <mpi.h>
#include <iostream>

#include "slurp_file.h"

int main(int argc, char** argv) {
    if (argc <2){
        std::cerr << "Filename is a required argument";
        return 0;
    }
    char *filename = argv[1];
    int numtasks, taskid;

    MPI_Init(&argc, &argv);
    MPI_Comm_size (MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank (MPI_COMM_WORLD, &taskid);

    int rank;
    std::vector<data_t> v;
    v.resize(numtasks);
    slurp_file_line(filename, taskid, v);

    int max = 0;
    for (auto const& v_: v) {
        if (v_ > max) {
            max = v_;
        }
    }

    std::cout << "MAX of processor with rank " << taskid << " is " << max << "\n";    

    MPI_Finalize();
    return 0;
}
