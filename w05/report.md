## Task 1

1. List all the options that `mpicxx` wrapper passes to C++ compilers.

Do it by calling `mpicxx -show`, they are:

```
g++ -I/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/include -L/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib/release_mt -L/curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib -Xlinker --enable-new-dtags -Xlinker -rpath -Xlinker /curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib/release_mt -Xlinker -rpath -Xlinker /curc/sw/intel/16.0.3/impi/5.1.3.210/intel64/lib -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/5.1/intel64/lib/release_mt -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/5.1/intel64/lib -lmpicxx -lmpifort -lmpi -lmpigi -ldl -lrt -lpthread
```


## Task 2

6. Summary and evaluation for `Amdahl's Law in the Multicore Era`.

Amdahl's law allows us to answer to what extend a sequential algorithm for a problem of size `n` can be speeded up. The law makes two simplifying assumptions. Firstly, it assumed that the operations can be evenly spread among processors, which gives maximum speedup. Secondly, it assumed that the time of parallel overhead, including the cost of communication between processors, is small among others, and can be neglected. In practice, these simplifying assumptions rarely met, and the Amdahl's law makes an upper bound for the speedup (an overly-optimistic one).

Based on  Amdahl's law, the construction of parallel computers that speed computation up is questioned. It implicates that the maximum speedup that could be achieved does not depend on the number of processors used, but on the amound of operations that must be performed sequentially in a parallel algorithm.

The paper explains how we might build multicore architectures such that we could gain more flexibity in the tradeoffs between sequential and parallel computations. Specifically, this paper discusses about the symmetric , asymmetric, and dynamic multicore chips in relevance to this.


