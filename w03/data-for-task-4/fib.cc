#include <omp.h>
#include <iostream>

long fib(long n) {
    long i(0), j(0);
    if (n<2) return 1;

    #pragma omp task shared(i)
    i = fib(n-1);
    std::clog << "i = " << i << "\n";

    #pragma omp task shared(j)
    j = fib(n-2);
    std::clog << "j = " << j << "\n";

    /* ... more code ... */
    #pragma omp taskwait
    {
        return i + j;
    }
}

int main(int argc, char** argv){
    long n = (long) atoi(argv[1]);
    long v;

    #pragma omp parallel shared (n, v)
    {
        #pragma omp single /* why single? */
        {
            v = fib(n);
            std::clog << "v = " << v << "\n";
        }
    }   
        std::clog << "The fib number is " << v << "\n";
	return v;
}


