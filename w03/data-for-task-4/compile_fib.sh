#!/bin/bash

#module load gcc

g++-8 -c -fopenmp fib.cc
g++-8 -fopenmp fib.o -o fib.exe

./fib.exe 10  # this should output fib_num = 89.
