#include <math.h>
#include <cassert>
#include "quicksort.h"
#include "omp-utils.h"

#define NWS 5e4   /* minimum size for workshare  */
#define NTSK 5e4  /* size to switch to tasks     */
#define NMIN 50   /* minimum size for final task */

void scan_sequential(std::vector<int>& x) {
    unsigned int n = x.size();
    for (unsigned int i = 1; i < n; ++i) {
        x[i] += x[i - 1];
    }
}

void scan_parallel(std::vector<int>& x) {
    size_t n = x.size();
    if (n < 2) {
        return;
    }
    const size_t num_p = omp_get_max_threads();
    if (n < num_p) {
        for (unsigned int i = 1; i < n; ++i) {
            x[i] += x[i - 1];
        }
        return;
    }

    std::vector <size_t> ps;
    ps.resize(num_p);

    #pragma omp parallel default(none) firstprivate(x, n, ps) 
    {
        size_t s = floor(1.0 * n / num_p);
        size_t r = omp_get_thread_num();

        size_t offset = r * s;
        if (r == num_p - 1) {
            s = n - offset;
        }

        for (size_t i = 1; i < s; ++i) {
            x[offset + i] += x[offset + i - 1];
        }
        ps[r] = x[offset + s - 1];
        #pragma omp barrier
        #pragma omp single
        for (size_t i = 1; i < num_p; ++i) {
            ps[i] += ps[i - 1];
        }

        #pragma omp barrier
        if (r > 0) {
            for (size_t i = 0; i < s; ++i) {
                x[offset + i] += ps[r - 1];
            }
        }
        ps.clear();
    }
}

void partition(const vec_t &x, const size_t n, const long pivot, vec_t& L, size_t &lsz, vec_t& H, size_t &hsz) {

    std::vector<int> c;
    c.resize(n, 0);

    std::vector<int> l;
    l.resize(n, 0);

    std::vector<int> h;
    h.resize(n, 0);

    #pragma omp parallel for default(none) firstprivate(c, l, h) shared(x) if (n > NWS)
    for (size_t ii = 0; ii < n; ++ii) {
        l[ii] = c[ii] = x[ii]<=pivot;
        h[ii] = !l[ii];
    }

    // function pointer to scan
    using scan_t = void (*)(std::vector<int>&);
    scan_t scan = (n > NWS) ? scan_parallel : scan_sequential;

    scan(l);
    scan(h);

    lsz = l[n-1];
    hsz = h[n-1];
    assert (lsz + hsz == n);

    if (lsz > 0) {
        L.resize(lsz);
    }
    
    if (hsz > 0) {
        H.resize(hsz);
    }

    vec_t Lp{L};
    vec_t Hp{H};

    #pragma omp parallel for default(none) firstprivate(l, h, c, Lp, Hp) shared(x) if (n > NWS)
    for (size_t ii = 0; ii < n; ++ii)
        if (c[ii]) {
            Lp[l[ii] - 1] = x[ii];
        } else {
            Hp[h[ii] - 1] = x[ii];
        }

    c.clear();
    l.clear();
    h.clear();

    return;
}

void quicksort_tsk(const vec_t &x, vec_t &y){
    size_t n = x.size();
    if (n < 2) {
        for (size_t ii = 0; ii < n; ++ii) {
            y[ii] = x[ii];    
        } 
        return;
    }

    long pivot = x[n - 1];
    vec_t L, H;

    size_t lsz, hsz;

    partition(x, n - 1, pivot, L, lsz, H, hsz);
    
    y[lsz] = pivot;

#pragma omp task default(none) firstprivate(L, y) if (n > NMIN)
    {
        quicksort_tsk(L, y);
        L.clear();
    }

// #pragma omp task default(none) firstprivate(H, lsz, y) if (n > NMIN) 
//     {
//         quicksort_tsk(H, y + lsz + 1);
//         H.clear();
//     }
    return;
}

void quicksort(const vec_t &x, vec_t &y){
    size_t n = x.size();
    if (n < 2) {
        for (size_t ii = 0;ii < n; ++ii) {
            y[ii] = x[ii];  
        } 
        return;
    }

    if (n < NTSK){
        #pragma omp parallel default(none) firstprivate(x, y) 
        {
            #pragma omp single nowait
            quicksort_tsk(x, y);
        }
        return;
    }

    long pivot = x[n - 1];
    vec_t L, H;

    size_t lsz, hsz;
    partition(x, n-1, pivot, L, lsz, H, hsz);
    y[lsz] = pivot;

    quicksort(L, y);
    // quicksort(H, y + lsz + 1);

    L.clear();
    H.clear();
}