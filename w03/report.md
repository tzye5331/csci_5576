## Task 1.

*1. Done. The files are in the commit.

*2. The first line of the `makefile` specifies the output excutable. If there is already a file with the same file name in the directory, the `make` command is not executable. The target `clean`, which constitutes the `make clean` command, will remove the executables and intermediate object files. This ensures that the updated object files and the excutable will be generated in the next run of make.


## Task 3.

*3. When running `make` after `make clean`, the printed value is: 

```
g++   -I. -c -o main.o main.cc
g++   -I. -c -o message.o message.cc
g++  -I. -o output main.o message.o
```


*4. After modifying `message.cc` and run `make`, the printed value is:

```
g++   -I. -c -o message.o message.cc
g++  -I. -o output main.o message.o
```

This indicates that `main.o` remains unchanged, but `make` detects change in `message.cc`, so it only compiles the `message.o` part of the object file, and then do the link.

## Task 4.

*5. We have to add the `#pragma omp taskwait` line. It will specify a wait time for the child tasks in each thread to be finished. 

## Task 5.

*1. Our strategy will depend on the size of work. Specifically, we strive for optimism between task parallelism and data parallelism. 

We can break down the `quicksort` task into (1) scan (repetitively compare values); (2) swap; (3) partition. 

For `scan`, we can always apply data parallelism (or, workshare).

For `swap`, if the work size is small, we do not need workshare.

For `partition`, only when the size of work is larger than the number of available processors, we can apply the `workshare`, and apply data parallelism. Otherwise, we introduce the overhead by copying the data within each thread.

*2. The code files are in `data-for-task-5`.

*3. Although I have not produced a figure, I expect that for strong scaling, the efficiency scales with the number of threads used. That is, the more processors, the better.

On the other hand, for weak scaling, the efficiency remains a constant.

*4. 


## Task 6.

*6. Depending on the number of processors availble, the matrix multiplication task has different `optimal scheduling`, `parallel work`, and `depth`.

For number of processors >= n^3, we can compute all the multiplications simultaneously, leaving only additions to go. The work is O(n^3), the depth is O(log(n)), and the optimal scheduling is Tp(n) <= floor(n^3 / p) + log(n).

For number of processors < n^3, the work remains the same, but we have fewer resources, so we cannot compute the multiplications simultaneously. In this case, the depth of the multiplication step is n^3 / p = n^3 / 2^q = 2^(3k-q). The depth for the sum also depends on `p`, for the partial sums, it is O(log(n)); to add all partial sums together, the depth is n^2 / p = 2^(2k-q). Hence, for the addition, the depth is of order O(n^2). Finally, the scheduling becomes Tp(n) <= floor(n^3 / p) + n^2 / p + log2(n).

*7. 

1. First, use `n` processors to compare every element `a_i` in `A` to `x` simultaneously, returning `1` if `a_i` is less than or equal to `x` or `0` otherwise. This list will be written in memory (not in-place). Then, perform a parallel sum with total work O(n) on these stored `1`'s and `0`'s to find the rank.

2. We can determine rank(x:A) with o(n) operations in log(n) time using a binary search on a PRAM. This is a EREW PRAM.




