#include <vector>
#include <functional>
#include <mpi.h>
#include <cmath>

// Some utlity classes and typedefs
#define DIM 3

typedef double real_t;

union point_t
{
    real_t coord[3];
    struct {
        real_t x;
        real_t y;
        real_t z;
    };
};

using real_t = double;

// define a type for matvec with vector input and output
using matvec_t = void (const std::vector<real_t>&, std::vector<real_t>&);

typedef std::vector<real_t> vec_t;
typedef std::vector<point_t> grid_t;



/*
 * Set up the grid communicator for the Poisson problem and samples
 * the correct portion of the domain
 *
 * @comm input communicator, typically MPI_COMM_WORLD
 * @n problem size, there are n sample points along each axis
 * @grid_comm the grid communicator (output)
 * @x sample points assigned to the current MPI process based on its grid coordinates (output)
 *
 */
void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm, grid_t &x);

/*
 * @grid_comm grid communicator returned by poisson_setup
 * @a value of coefficient a at points x
 * @v candidate solution at points x
 * @lv the matrix-vector product of the discrete Poisson's operator
 */
void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv);

/*
 * @comm mpi communicator
 * @mv the matvec operator
 * @v the input vector to matvec
 * @rhs the right-hand-side of the linear operator
 * @res the point-wise residual (output)
 * @res_norm the L2 norm of the residual vector `res`
 */
void residual(const MPI_Comm &comm, matvec_t &mv, vec_t &v, const vec_t &rhs, vec_t &res, real_t &res_norm);
