
#include "poisson.h"
#include <iostream>
#include <cstdio>
#include <cmath>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
    
    int size;
 	MPI_Comm_size(comm, &size); 

    int dims[DIM] = {0};    
    int periods[DIM] = {0};

  	MPI_Dims_create(size, DIM, dims);
    MPI_Cart_create(comm, DIM, dims, periods, 1, &grid_comm);
    

    int coords[DIM];
    int rank;
 	MPI_Comm_rank(grid_comm, &rank);
    MPI_Cart_coords(grid_comm, rank, DIM, coords);
    

    double iSize = n/dims[0];
    double jSize = n/dims[1];
    double kSize = n/dims[2];

    int iStart = coords[0]*iSize;
    int jStart = coords[1]*jSize;
    int kStart = coords[2]*kSize;

    int iRange = (iStart+iSize)>n? n : (iStart+iSize);
    int jRange = (jStart+jSize)>n? n : (jStart+jSize);
    int kRange = (kStart+kSize)>n? n : (kStart+kSize);

    for(int i= iStart ; i<iRange; i++){
		for(int j = jStart; j<jRange; j++){
			for(int k = kStart; k<kRange; k++){
				point_t pt = {i,j,k};
				x.push_back(pt);
			}
		}
	}	
}

int get_Index(double i, double j, double k, double iRange,double jRange) 
{ 
        return (int)(i*iRange*jRange + j*jRange + k); 
} 

double shift_cart(const MPI_Comm &grid_comm, int dimension, int  direction, double vVal) 
{ 
        int src, dst; 
        MPI_Cart_shift(grid_comm, dimension, direction,&src,&dst);
 
        if(dst!= MPI_PROC_NULL) 
        	MPI_Send(&vVal, 1, MPI_DOUBLE, dst, 0, grid_comm); 
 
       	MPI_Cart_shift(grid_comm, dimension,direction*(-1), &src, &dst);
 
        if(dst != MPI_PROC_NULL) { 
        	double valRecieved; 
        	MPI_Status status; 
            MPI_Recv(&valRecieved, 1, MPI_DOUBLE, dst, 0, grid_comm, &status); 
            return valRecieved; 
        } 
        return 0; 
} 



void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv) 
{ 
 
    int dimensions[DIM] = {0};  
    int periods[DIM] = {0};  
    int coords[DIM] = {0}; 
 
    MPI_Cart_get(grid_comm, DIM, dimensions, periods, coords); 
 
    int iSize = n/dimensions[0]; 
    int jSize = n/dimensions[1]; 
    int kSize = n/dimensions[2]; 
    double h = 1.0/((double)n-1.0); 
    int iStart = coords[0]*iSize;
    int jStart = coords[1]*jSize;
    int kStart = coords[2]*kSize;

    int iRange = (iStart+iSize)>n? (n-iStart) : (iSize);
    int jRange = (jStart+jSize)>n? (n-jStart) : (jSize);
    int kRange = (kStart+kSize)>n? (n-kStart) : (kSize);
 
    for(int i=0; i<iRange; i++){ 
       for(int j=0; j<jRange; j++){ 
           for(int k=0; k<kRange; k++){ 
                int index = get_Index(i,j,k,iRange,jRange); 
 				double lvVal = 0; 
                double vVal = v[index]; 
 
                if(i==0) { 
                    lvVal = lvVal + shift_cart( grid_comm, 0, -1,vVal) + v[get_Index(i+1, j, k, iRange, jRange)]; 
                }else if(i==iRange-1){ 
                    lvVal = lvVal + shift_cart( grid_comm, 0, 1,vVal) + v[get_Index(i-1, j, k, iRange, jRange)]; 
                }else{ 
                    lvVal = lvVal + v[get_Index(i+1, j, k, iRange, jRange)] + v[get_Index(i-1, j, k, iRange, jRange)]; 
                } 
 
                if(j==0) { 
                    lvVal = lvVal + shift_cart(grid_comm, 1, -1,vVal) + v[get_Index(i, j+1, k, iRange, jRange)]; 
                }else if(j==jRange-1){ 
                    lvVal = lvVal + shift_cart( grid_comm, 1, 1, vVal) + v[get_Index(i, j-1, k, iRange, jRange)]; 
                }else{ 
                    lvVal = lvVal + v[get_Index(i, j+1, k, iRange, jRange)] + v[get_Index(i, j-1, k, iRange, jRange)]; 
                } 
 
                if(k==0) { 
                    lvVal = lvVal + shift_cart( grid_comm, 2, -1,vVal) + v[get_Index(i, j, k+1, iRange, jRange)];  
                }else if(k==kRange-1){ 
                    lvVal = lvVal + shift_cart( grid_comm, 2, 1,vVal) + v[get_Index(i, j, k-1, iRange, jRange)];  
                }else{ 
                    lvVal = lvVal + v[get_Index(i, j, k+1, iRange, jRange)] +v[get_Index(i, j, k-1, iRange, jRange)]; 
                } 
                
                lvVal = a[index]*vVal - (lvVal-(6*vVal))/(h*h); 
                lv[index] = lvVal;
            } 
        } 
    } 
 
}


void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs, 
              vec_t &res, real_t &res_norm) 
{ 
     
    vec_t lv(v.size()); 
    mv(v,lv); 
    
    double residue = 0; 
    for(int i=0; i<v.size(); i++){  
        res[i] = rhs[i] - lv[i]; 
        residue= residue + res[i]*res[i];  
    } 

    MPI_Barrier(comm); 
    MPI_Reduce(&residue, &res_norm, 1, MPI_DOUBLE, MPI_SUM, 0,comm); 
} 