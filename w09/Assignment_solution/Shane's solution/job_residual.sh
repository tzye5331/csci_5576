#!/bin/bash

#SBATCH --time=0:55:00     # walltime, abbreviated by -t
#SBATCH --nodes=1          # number of cluster nodes, abbreviated by -N
#SBATCH -o residual.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e residual.err     # name of the stderr redirection file
#SBATCH --ntasks=8         # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo Begin set n = {2,4,8,16,32,64,128,256,512} for residual analysis
for i in {2,4,8,16,32,64,128,256,512}; 
do 
	mpirun -n 8 ./test_poisson.exe $i
done
