#!/bin/bash

#SBATCH --time=0:55:00     # walltime, abbreviated by -t
#SBATCH --nodes=3          # number of cluster nodes, abbreviated by -N
#SBATCH -o weak.out     # name of the stdout redirection file, using the job number (%j)
#SBATCH -e weak.err     # name of the stderr redirection file
#SBATCH --ntasks=64         # number of parallel process
#SBATCH --qos debug        # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo Begin set p = {8, 27, 64} for weak scaling
mpirun -n 8 ./test_poisson.exe 600
mpirun -n 27 ./test_poisson.exe 900
mpirun -n 64 ./test_poisson.exe 1200
