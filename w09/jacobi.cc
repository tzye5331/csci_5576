#include "poisson.h"
#include "jacobi.h"
#include <stdio.h>
#include <cmath>
#include <iostream>
#include <cassert>
#include <vector>
#include <functional>
#include <sys/time.h>
#include <stdlib.h>
#include <chrono>

#define pi 3.1415926


void jacobi_solver(const MPI_Comm &comm, std::function <matvec_t> mv,
                   std::vector <real_t> &diag, std::vector <real_t> &rhs,
                   std::vector <real_t> &u0, real_t eps_r, real_t eps_a, int k_max,
                   std::vector <real_t> &u_final, int &k, int &n) {
    int r;
    real_t loc_residual = 0;
    std::vector <real_t> res;
    res.resize(u0.size());
    u_final.resize(u0.size());
    real_t total_residual = 0;
    real_t sqrt_total_residual = -1;
    real_t initial_residual = 0;
    int term_flag = 0;

    while (1) {
        if (term_flag) {
            return;
        }

        MPI_Comm_rank(comm, &r);

        // calling Laplacian here (matvec function)
        mv(u0, u_final);

        // calculate residual here for each processor
        for (int i = 0; i < u_final.size(); i++) {
            res[i] = rhs[i] - u_final[i];
            loc_residual += (res[i] * res[i]);
        }

        // Waiting for all the processes here to complete.
        MPI_Barrier(comm);

        // Reduce all the local residual to the root node (the 0th processor).
        MPI_Reduce(&loc_residual, &total_residual, 1, MPI_DOUBLE, MPI_SUM, 0, comm);

        std::clog << "loc_residual: " << loc_residual << "\n";

        MPI_Barrier(comm);


        if (r == 0) {
            if (sqrt_total_residual == -1) {
                sqrt_total_residual = sqrt(total_residual);
                initial_residual = sqrt_total_residual;
            } else {
                // calculate here if the sqrt of the total residual is within the limits of the expected value.
                k++;
                sqrt_total_residual = sqrt(total_residual);
                std::clog << "squared total residual is: " << sqrt_total_residual << " (does it become smaller?) \n";
                if ((sqrt_total_residual < ((eps_r * initial_residual) + eps_a)) || (k > k_max)) {
                    term_flag = 1;  // end the jacobi_solver

                    printf("K : %d\n", k);
                    MPI_Bcast(&term_flag, 1, MPI_INT, 0, comm);
                }
            }
        }

        // Obtaining u+ (the updated next value of u) here
        int length = int(std::cbrt(u0.size()));
        vec_t D, D_inv;
        double h = 1 / (n - 1);
        for (int i = 0; i < u0.size(); i++) {
            if (floor(i / length) == i % length && i % length == i % (length * length)) {
                D.push_back(12 - h * h / 6);
                D_inv.push_back(1 / (12 - h * h / 6));
            } else {
                D.push_back(0);
                D_inv.push_back(0);
            }
 //           std::clog << "before (u[0], u[1], u[2]) = (" << u0[0] << ", " << u0[1] << ", " << u0[2] << "); \n";
       //     u0[i] = D_inv[i] * (res[i] + D[i] * u0[i]);
            u0[i] = (res[i] + diag[i] * u0[i]) / diag[i];
   //         std::clog << "after (u[0], u[1], u[2]) = (" << u0[0] << ", " << u0[1] << ", " << u0[2] << "); \n";
            res[i] = 0;
            u_final[i] = 0;
        }

        loc_residual = 0;
        total_residual = 0;
        MPI_Barrier(comm);

    }
}

int main(int argc, char *argv[]) {
    using namespace std::placeholders;

    std::vector <point_t> x;
    MPI_Init(&argc, &argv);

    int r;
    MPI_Comm_rank(MPI_COMM_WORLD, &r);

    if (argc < 2) {
        if (r == 0) {
            printf("Usage: %s n\n", argv[0]);
            MPI_Finalize();
            exit(1);
        }
    }

    int n = atoi(argv[1]);

    MPI_Comm grid_comm;
    MPI_Comm start_comm = MPI_COMM_WORLD;
    poisson_setup(start_comm, n, grid_comm, x);

    std::vector <real_t> a(0);
    std::vector <real_t> u(0);
    std::vector <real_t> u_final(0);
    std::vector <real_t> lu(0);
    std::vector <real_t> diag(0);
    std::vector <real_t> f(0);

    real_t eps_r = std::pow(10, -5);
    real_t eps_a = std::pow(10, -12);
    int k_max = 100;
    int k = 0;

    for (auto p: x) {
        a.push_back(12);
        u.push_back(sin(4 * pi * p.x) * sin(10 * pi * p.y) * sin(14 * pi * p.z));
        diag.push_back(6.0);
        f.push_back((real_t) sin(2 * pi * p.x) * sin(12 * pi * p.y) * sin(8 * pi * p.z));
    }

    lu.resize(u.size());
    u_final.resize(u.size());

    std::function <matvec_t> mv = std::bind(poisson_matvec, grid_comm, n, a, _1, _2);

    // start timer
    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    jacobi_solver(grid_comm, mv, diag, f, u, eps_r, eps_a, k_max, u_final, k, n);
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = end - start;


    std::cout << "Number of times it has iterated (k) :" << k << "\n";
    std::cout << "Elapsed time: "<< time_span.count() <<" seconds"<<std::endl;

    MPI_Finalize();
}
