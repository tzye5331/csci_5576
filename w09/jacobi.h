#include <iostream>
#include <vector>
#include <functional>


using real_t = double;
using matvec_t = void(const std::vector <real_t> &, std::vector <real_t> &);


/*
 * @comm MPI communicator (to compute residual)
 * @mv the matvec
 * @diag diagonal component of the matvec
 * @rhs the right hand side
 * @u0 initial guess
 * @eps_r relative tolerance
 * @eps_a absolute tolerance
 * @m check residual every m iterations
 * @k_max the maximum number of iteration
 * @u_final return value
 * @k the number of iteration taken
 * @n
 */
void jacobi_solver(const MPI_Comm &comm, std::function<matvec_t> mv,
                 std::vector<real_t> &diag, std::vector<real_t> &rhs,
                 std::vector<real_t> &u0, real_t eps_r, real_t eps_a, int k_max,
                 std::vector<real_t> &u_final, int &k, int &n);
