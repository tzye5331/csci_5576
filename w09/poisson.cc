/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */

#include "poisson.h"
#include <iostream>

void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{
    int world_size;
    MPI_Comm_size(comm, &world_size);
    int world_rank;
    MPI_Comm_rank(comm, &world_rank);
//	printf("setup (%i of %i)\n", world_rank, world_size);

    // specify dimensionality of cartesian grid
    int n_dims(3);
    int dims[n_dims] = { };
    // determine coordinate bounds for each dimension such that
    // processors can be arranged in a grid
    MPI_Dims_create(world_size, n_dims, dims);
    if(world_rank==0){
        printf("Grid dimensions are %i by %i by %i\n",
               dims[0], dims[1], dims[2]);;
    }
    // assume non-periodic lattice (0 -> false)
    int periodic[n_dims] = { };
    // allow indices to be reordered
    bool reorder(true);
    // create cartesian grid communicator
    MPI_Cart_create(comm, n_dims, dims, periodic, reorder, &grid_comm);

    // find number of datapoints per processor (m) in each dimension
    int m[n_dims] = { };
    int m_tot(1);
    for(int i(0); i < n_dims; i++){
        if(n % dims[i] != 0){
            if(world_rank == 0){
                printf("Error: n must be divisible by all grid dimensions\n");
            }
            exit(1);
        }
        m[i] = n / dims[i];
        m_tot *= m[i];
    }

    // get coordinates of local processor in communication grid
    int grid_coord[n_dims] = { };
    MPI_Cart_coords(grid_comm, world_rank, n_dims, grid_coord);

    // find distance per datapoint on the numberline, i.e. h
    double h = (double) 1 / (n - 1);
    // resize x so that it can hold all local coordinates
    x.resize(m_tot);
    if(n_dims == 3){
        // scan over z dimension
        for(int k(0); k < m[2]; k++){
            // get global z-coordinate
            int i_z = (grid_coord[2] * m[2]) + k;
            double z_ = (double)i_z * h;
            // scan over y dimension
            for(int j(0); j < m[1]; j++){
                // get global y-coordinate
                int i_y = (grid_coord[1] * m[1]) + j;
                double y_ = (double)i_y * h;
                // scan over x dimension
                for(int i(0); i < m[0]; i++){
                    // get global x-coordinate
                    int i_x = (grid_coord[0] * m[0]) + i;
                    double x_ = (double)i_x * h;
                    // convert 3-D coord into unique serial ID
                    int serial_id = (m[1] * m[0])*k + m[0]*j + i;
                    // store coordinates in local x array
                    x[serial_id].x = x_;
                    x[serial_id].y = y_;
                    x[serial_id].z = z_;
                }
            }
        }
    }
    else{
        if(world_rank == 0)
            printf("Error: code not set up for n_dim != 3\n");
        exit(1);
    }
}

void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, vec_t &v, vec_t &lv)
{
    // find distance per datapoint on the numberline (h)
    double h = (double) 1 / (n - 1);

    // get details from the grid communicator passed to us
    int n_dims = 3;
    int dims[n_dims];
    int periodic[n_dims];
    int grid_coord[n_dims];
    MPI_Cart_get(grid_comm, n_dims, dims, periodic, grid_coord);

    // convert grid coord into world rank of processor
    int world_rank;
    MPI_Cart_rank(grid_comm, grid_coord, &world_rank);

    // find number of datapoints per processor (m) in each dimension
    int m[n_dims] = { };
    int m_tot(1);
    for(int i(0); i < n_dims; i++){
        m[i] = n / dims[i];
        m_tot *= m[i];
    }

    // if m_tot doesn't match the size of v, something went wrong
    int n_entries = v.size();
    if(m_tot != n_entries){
        if(world_rank == 0)
            printf("Error in poisson_matvec\n");
        exit(1);
    }

    // use MPI_Cart_shift to get world ranks of front/rear neighbors
    int source[n_dims];
    int dest[n_dims];
    for(int i(0); i < n_dims; i++){
        int source_l, dest_l;
        MPI_Cart_shift(grid_comm, i, 1, &source_l, &dest_l);
        source[i] = source_l;
        dest[i] = dest_l;
    }

    MPI_Status st;
    // Create local copies of neighboring faces
    std::vector<vec_t> upper_neighb;
    std::vector<vec_t> lower_neighb;
    upper_neighb.resize(n_dims);
    lower_neighb.resize(n_dims);
    // first index (i) gives dimension; second index (q) is
    // serialized coord of neighbor elements in that dimension
    // coordinate shift is is cyclic, so we get the following planes:
    // i = 0: X->[Y,Z]
    // i = 1: Y->[Z,X]
    // i = 2: Z->[X,Y]
    for(int i(0); i < n_dims; i++){
        int j_eff = (i + 1) % n_dims;
        int k_eff = (i + 2) % n_dims;
        // Initialize the upper/lower faces that will be sent to neighbors
        vec_t lower_face;
        vec_t upper_face;
        lower_face.resize(m[j_eff]*m[k_eff]);
        upper_face.resize(m[j_eff]*m[k_eff]);
        upper_neighb[i].resize(m[j_eff]*m[k_eff]);
        lower_neighb[i].resize(m[j_eff]*m[k_eff]);
        // run through new effective coordinates; get element IDs
        // and map them to appropriate serial indices in face arrays
        for(int q(0); q < m[j_eff]; q++){
            for(int p(0); p < m[k_eff]; p++){
                int serial_index = q*m[k_eff] + p;
                int lower_id;
                int upper_id;
                switch(i){
                    case 0:
                        lower_id = m[1]*m[0]*p + m[0]*q;
                        upper_id = m[1]*m[0]*p + m[0]*q + (m[0] - 1);
                        break;
                    case 1:
                        lower_id = m[1]*m[0]*q + p;
                        upper_id = m[1]*m[0]*q + m[0]*(m[1] - 1) + p;
                        break;

                    case 2:
                        lower_id = m[0]*p + q;
                        upper_id = m[1]*m[0]*(m[2] - 1) + m[0]*p + q;
                        break;
                }
                lower_face[serial_index] = v[lower_id];
                upper_face[serial_index] = v[upper_id];
            }
        }
        // send neighboring planes in a ring-like fashion
        // processor 0 sends first, then 1, then 2, etc.
        // upper face is sent first
        if(world_rank != 0
           && source[i] != -1){
            MPI_Recv(&lower_neighb[i][0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     source[i], 2*i + 1, grid_comm, &st);
        }
        if(dest[i] != -1){
            MPI_Send(&upper_face[0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     dest[i], 2*i + 1, grid_comm);
        }
        if(world_rank == 0
           && source[i] != -1){
            MPI_Recv(&lower_neighb[i][0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     source[i], 2*i + 1, grid_comm, &st);
        }
        // lower face is sent second
        if(world_rank != 0
           && dest[i] != -1){
            MPI_Recv(&upper_neighb[i][0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     dest[i], 2*i, grid_comm, &st);
        }
        if(source[i] != -1){
            MPI_Send(&lower_face[0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     source[i], 2*i, grid_comm);
        }
        if(world_rank == 0
           && dest[i] != -1){
            MPI_Recv(&upper_neighb[i][0], m[j_eff]*m[k_eff], MPI_DOUBLE,
                     dest[i], 2*i, grid_comm, &st);
        }
    }

    // calculate laplacian using method of finite differences
    vec_t lap_v;
    lap_v.resize(n_entries);
    // scan over z dimension
    for(int k(0); k < m[2]; k++){
        // get global z-coordinate
        int i_z = (grid_coord[2] * m[2]) + k;
        // ignore boundary sites
        if(i_z > 0 && i_z < n - 1){
            // scan over y dimension
            for(int j(0); j < m[1]; j++){
                // get global y-coordinate
                int i_y = (grid_coord[1] * m[1]) + j;
                // ignore boundary sites
                if(i_y > 0 && i_y < n - 1){
                    // scan over x dimension
                    for(int i(0); i < m[0]; i++){
                        // get x-coordinate
                        int i_x = (grid_coord[0] * m[0]) + i;
                        // ignore boundary sites
                        if(i_x > 0 && i_x < n - 1){
                            // convert 3-D coord into unique serial ID
                            int serial_id = (m[1] * m[0])*k + m[0]*j + i;
                            // get value of function at this point
                            real_t u_xyz = v[serial_id];
                            real_t u_xfwd, u_xbck,
                                    u_yfwd, u_ybck,
                                    u_zfwd, u_zbck;
                            // Get rear neighbor in x-dimension
                            if(i > 0){
                                int id_bck = serial_id - 1;
                                u_xbck = v[id_bck];
                            }
                                // use neighb list
                            else{
                                int neighb_id = j*m[2] + k;
                                u_xbck = lower_neighb[0][neighb_id];

                            }

                            // Get front neighbor in x-dimension
                            if(i < m[0] - 1){
                                int id_fwd = serial_id + 1;
                                u_xfwd = v[id_fwd];
                            }
                                // use neighb list
                            else{
                                int neighb_id = j*m[2] + k;
                                u_xfwd = upper_neighb[0][neighb_id];
                            }

                            // Get rear neighbor in y-dimension
                            if(j > 0){
                                int id_bck = serial_id - m[0];
                                u_ybck = v[id_bck];
                            }
                                // use neighb list
                            else{
                                int neighb_id = k*m[0] + i;
                                u_ybck = lower_neighb[1][neighb_id];
                            }

                            // Get front neighbor in y-dimension
                            if(j < m[1] - 1){
                                int id_fwd = serial_id + m[0];
                                u_yfwd = v[id_fwd];
                            }
                                // use neighb list
                            else{
                                int neighb_id = k*m[0] + i;
                                u_yfwd = upper_neighb[1][neighb_id];
                            }

                            // Get rear neighbor in z-dimension
                            if(k > 0){
                                int id_bck = serial_id - (m[1]*m[0]);
                                u_zbck = v[id_bck];
                            }
                                // use neighb list
                            else{
                                int neighb_id = i*m[1] + j;
                                u_zbck = lower_neighb[2][neighb_id];
                            }

                            // Get front neighbor in z-direction
                            if(k < m[2] - 1){
                                int id_fwd = serial_id + (m[1]*m[0]);
                                u_zfwd = v[id_fwd];
                            }
                                // use neighb list
                            else{
                                int neighb_id = i*m[1] + j;
                                u_zfwd = upper_neighb[2][neighb_id];
                            }

                            // calculate the laplacian at this point
                            lap_v[serial_id] = u_xfwd + u_xbck + u_yfwd +
                                               u_ybck + u_zfwd + u_zbck -
                                               (6 * u_xyz);
                            lap_v[serial_id] /= (h * h);
                        }
                    }
                }
            }
        }
    }
    // calculate the overall linear differential operator acting on v
    // i.e. calculate lv = (a*v - lap_v)
    lv.resize(n_entries);
    for(int k(0); k < m[2]; k++){
        int i_z = (grid_coord[2] * m[2]) + k;
        for(int j(0); j < m[1]; j++){
            int i_y = (grid_coord[1] * m[1]) + j;
            for(int i(0); i < m[0]; i++){
                int i_x = (grid_coord[0] * m[0]) + i;
                int index = m[1]*m[0]*k + m[0]*j + i;
                // let lv = -1 on the boundary so it can
                // be ignored in the residual calculation
                if(i_x == 0 || i_x == n - 1
                   || i_y == 0 || i_y == n - 1
                   || i_z == 0 || i_z == n - 1){
                    lv[index] = -1;
                }
                    // otherwise, lv = a*v - lap_v for all points
                else{
                    lv[index] = a[index]*v[index] - lap_v[index];
                }
            }
        }
    }
}

void residual(const MPI_Comm &comm, matvec_t &mv, vec_t &v, const vec_t &rhs,vec_t &res, real_t &res_norm)
{
    int world_size;
    MPI_Comm_size(comm, &world_size);
    int world_rank;
    MPI_Comm_rank(comm, &world_rank);

    int n_entries = v.size();
    res.resize(n_entries);

    vec_t lv;
    // find lv using our implicit matvec function
    mv(v, lv);

    // calculate residual norm of lv compared to known solution (rhs)
    res_norm = 0;
    for(int i(0); i < n_entries; i++){
        // only get residual if lv != 1, i.e. if we are not on boundary
        if(lv[i] != -1){
            res[i] = rhs[i] - lv[i];
            res_norm += res[i] * res[i];
        }
    }
    res_norm = sqrt(res_norm);
}
