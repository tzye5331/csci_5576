// Lab 3, Task 4: OpenMP Task parallelism

#include <stdio.h>
#include "fib.h"


// recursive Fibonacci, serial
int fib(int n) {
    if (n <= 1) {
        return n;
    }
    return fib(n-1) + fib(n-2);
}

// recursive Fibonacci, openmp
long fib(long n) {
    long i(0), j(0);
    if (n < 2) {
        return 1;
    }

    #pragma omp task shared(i)
    i = fib(n - 1);

    #pragma omp task shared(j)
    j = fib(n - 2);
    /// what else is required here?? ///
    return i + j;
}

int main() {
    long n = fib(10000);
    printf("%ld \n", n);
    return 0;
}

