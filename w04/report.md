## Task 1

1. Done. See the updated files `semaphore.cc` and `pc.cc`. The output of `pc.exe` (after I finished Task 2) matches that of `task1.exe`, which is:
```$commandline
   1 00001
   1 00002
   1 00003
   1 00004
   1 00005
   1 00006
   1 00007
   1 00008
   1 00009
   1 00010
   1 00011
   1 00012
   1 00013
   1 00014
   1 00015
   1 00016
   1 00017
   1 00018
   1 00019
   1 00020

```
   
   
## Task 2

2-a. With only one lock, the variable `val_` can still become negative which is not allowed for a semaphore.
We need another lock, i.e. `zero_lock_`, to ensure that the thread decrements the variable only when it is greater than 0. 
Otherwise, the thread has to wait until other threads incremented the variable (using `post`) to release the `zero_lock_`.

2-b. Done.

2-c. Done.

2-d. Action on `val_` usually consists of reading `val_`’s value, incrementing the value that was read, and writing the result back into `val_`. 
But these three operations are not guaranteed to proceed atomically for `volatile` objects, so it’s possible that the component parts of the two increments of `val_` are interleaved as follows (say `val_` is initialized to `-1`):

    1. Thread 1 reads val_’s value, which is -1.
    2. Thread 2 reads val_’s value, which is still -1.
    3. Thread 1 increments the -1 it read to 0, then writes that value into val_.
    4. Thread 2 increments the -1 it read to 0, then writes that value into val_.
`val_`’s final value is therefore 0, even though it was incremented twice.

And actually this is not only the possible outcome. `val_`’s final value is, in general, not predictable, because `val_` is involved in a data race, and the compilers may generate code to do literally anything.

2-e. 

* To safely update `val_`, I used the `val_lock_` and `zero_lock_` in the constructor.
And I used `pragma parallel single` to ensure that the `zero_lock_` works as expected.

* To block when `val_` is zero, the thread must wait the release of the `zero_lock_` and the `val_lock_` (so that no other thread is updating the value) for it to update the `val_`.
It is designed to release the `val_lock_` whatever the `val_` variable is.

2-f. Done.

2-g. Done. The `pc.exe` code indeed prints out as expected, which is:

```commandline
 500 00001
 500 00002
 500 00003
 500 00004
 500 00005
 500 00006
 500 00007
 500 00008
 500 00009
 500 00010

```
 
## Task 3

3. The two variables `next_put` and `next_get` are subject to the race condition.
When two producer threads are working, they may try to update the `next_put` at the same time.
This is a race condition, which eventually `next_put` will be incremented by 1 instead of 2.
What is worse, after compiler optimization, we may obtain even more unpredictable behavior.

A similar situation happens when there are two consumers, where they try to update `next_get` in parallel.  

4. My output is (after setting `export OMP_NUM_THREADS=6`):
```commandline
 500 00001
 500 00002
 500 00003
 500 00004
 500 00005
 501 00006
 500 00007
 501 00008
 498 00009
 499 00010
 498 00011
 501 00012
 500 00013
 502 00014
 500 00015
 502 00016
 498 00017
 499 00018
 503 00019
 499 00020
 500 00021
 500 00022
 500 00023
 501 00024
 498 00025
 499 00026
 502 00027
 500 00028
 500 00029
 499 00030

```

Wherever there is a value higher than 500, it is because of the race condition on the `next_get` variable.
And, wherever there is a value less than 500, it's because of the race condition on the `next_put` variable. 

5. `put_lock_` and `get_lock_` of type `omp_lock_t` were added to mitigate this problem.

