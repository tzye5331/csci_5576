12. During linking, many errors were returned:

test_axpy.o: In function `main':
test_axpy.cc:(.text+0x3c): undefined reference to `operator new[](unsigned long)'
test_axpy.cc:(.text+0x69): undefined reference to `operator new[](unsigned long)'
test_axpy.cc:(.text+0x84): undefined reference to `std::cout'
test_axpy.cc:(.text+0x89): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0xbe): undefined reference to `std::cout'
test_axpy.cc:(.text+0xc3): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0xc8): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::endl<char, std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&)'
test_axpy.cc:(.text+0xd0): undefined reference to `std::ostream::operator<<(std::ostream& (*)(std::ostream&))'
test_axpy.cc:(.text+0xec): undefined reference to `operator delete[](void*)'
test_axpy.cc:(.text+0xff): undefined reference to `operator delete[](void*)'
test_axpy.cc:(.text+0x10e): undefined reference to `__cxa_begin_catch'
test_axpy.cc:(.text+0x118): undefined reference to `std::cout'
test_axpy.cc:(.text+0x11d): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0x122): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::endl<char, std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&)'
test_axpy.cc:(.text+0x12a): undefined reference to `std::ostream::operator<<(std::ostream& (*)(std::ostream&))'
test_axpy.cc:(.text+0x12f): undefined reference to `__cxa_end_catch'
test_axpy.cc:(.text+0x139): undefined reference to `__cxa_end_catch'
test_axpy.o: In function `__static_initialization_and_destruction_0(int, int)':
test_axpy.cc:(.text+0x172): undefined reference to `std::ios_base::Init::Init()'
test_axpy.cc:(.text+0x181): undefined reference to `std::ios_base::Init::~Init()'
test_axpy.o:(.eh_frame+0x13): undefined reference to `__gxx_personality_v0'
collect2: error: ld returned 1 exit status


I have to use `-fopenmp` when building the object file for `axpy.cc`. 
The program behavior did not change.

13. I guess multiple lines of:

 shasXXXX.rc.int.colorado.edu
 Calling axpy - Passed

will be printed, because the program is now running in parallel threads. 

