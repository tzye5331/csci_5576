1. It says that:

PATH   The search path for commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below).  A zero-length (null)  directory  name
              in the value of PATH indicates the current directory.  A null directory name may appear as two adjacent colons, or as an initial or trailing colon.  The default path is system-depen‐
              dent, and is set by the administrator who installs bash.  A common value is ``/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin''.

2. Normally `/lib` and `/usr/lib`.

3. An empty value. No value was assigned to $LD\_LIBRARAY\_PATH by default.
 
