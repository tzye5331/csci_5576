#!/bin/bash

#SBATCH --time=0:05:00 # walltime, abbreviated by -t
#SBATCH --nodes=1 # number of cluster nodes, abbreviated by -N
#SBATCH -o axpy-%j.out # name of the stdout redirection file, using the job number (%j)
#SBATCH -e axpy-%j.err # name of the stderr redirection file
#SBATCH --ntasks 1 # number of parallel process
#SBATCH --qos debug # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

./test_axpy.exe
