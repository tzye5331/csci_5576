11. I used `sacct` to check the status of the job.

For `*.out` it prints:

 shas0136.rc.int.colorado.edu
 Calling axpy - Passed
 
For `*.err`, nothing is printed.
