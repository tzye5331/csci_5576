#include "axpy.h"
#pragma omp parallel

void axpy(size_t n, double a, const double *x, double *y) throw (std::runtime_error) {
    #pragma omp for
    for (unsigned int i = 0; i < n; ++i) {
        y[i] = a * x[i] + y[i];
    }
};
