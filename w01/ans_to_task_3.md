8. To answer this question, type `env`.

The `intel` module sets environmental variables such as `CXX=icpc`, `LD=xild`, `FC=ifort`, and `CC=icc`.
The `gcc` module sets environmental variables such as `CXX=g++`, `FC=gfortran`, and `CC=gcc`.

