9. I choose to use `gcc`. The printed text is:
Using built-in specs.
COLLECT\_GCC=gcc
COLLECT\_LTO\_WRAPPER=/curc/sw/gcc/6.1.0/libexec/gcc/x86\_64-pc-linux-gnu/6.1.0/lto-wrapper
Target: x86\_64-pc-linux-gnu
Configured with: ../gcc-6.1.0/configure --prefix=/curc/sw/gcc/6.1.0 --enable-languages=c,c++,fortran,go --disable-multilib --with-tune=intel
Thread model: posix
gcc version 6.1.0 (GCC)

10. To answer this question, these files (`/w01/axpy.cc`, `/w01/compile.sh`) are included in the commit.
The bash script is for compiling the excutable, `test_axpy.exe`, which is not added in the commit.
 

