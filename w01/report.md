1. It says that:

PATH   The search path for commands.  It is a colon-separated list of directories in which the shell looks for commands (see COMMAND EXECUTION below).  A zero-length (null)  directory  name
              in the value of PATH indicates the current directory.  A null directory name may appear as two adjacent colons, or as an initial or trailing colon.  The default path is system-depen‐
              dent, and is set by the administrator who installs bash.  A common value is ``/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin''.

2. Normally `/lib` and `/usr/lib`.

3. An empty value. No value was assigned to $LD\_LIBRARAY\_PATH by default.
 
4. Done.

5. PATH is changed from `/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin` to `/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin`, and LD\_LIBRARY\_PATH is changed from `` to `/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64`.

6. New modules, for example, the one (`impi/17.3`) for MPI Implementations appear. There are also Compiler Dependent Applications that could be further loaded.

7. The hierarchical module system helps prevent loading conflicting dependencies, thus helps development and debugging.

8. To answer this question, type `env`.

The `intel` module sets environmental variables such as `CXX=icpc`, `LD=xild`, `FC=ifort`, and `CC=icc`.
The `gcc` module sets environmental variables such as `CXX=g++`, `FC=gfortran`, and `CC=gcc`.

9. I choose to use `gcc`. The printed text is:
Using built-in specs.
COLLECT\_GCC=gcc
COLLECT\_LTO\_WRAPPER=/curc/sw/gcc/6.1.0/libexec/gcc/x86\_64-pc-linux-gnu/6.1.0/lto-wrapper
Target: x86\_64-pc-linux-gnu
Configured with: ../gcc-6.1.0/configure --prefix=/curc/sw/gcc/6.1.0 --enable-languages=c,c++,fortran,go --disable-multilib --with-tune=intel
Thread model: posix
gcc version 6.1.0 (GCC)

10. To answer this question, these files (`/w01/axpy.cc`, `/w01/compile.sh`) are included in the commit.
The bash script is for compiling the excutable, `test_axpy.exe`, which is not added in the commit.
 
11. I used `sacct` to check the status of the job.

For `*.out` it prints:

 shas0136.rc.int.colorado.edu
 Calling axpy - Passed
 
For `*.err`, nothing is printed.
 
12. During linking, many errors were returned:

test_axpy.o: In function `main':
test_axpy.cc:(.text+0x3c): undefined reference to `operator new[](unsigned long)'
test_axpy.cc:(.text+0x69): undefined reference to `operator new[](unsigned long)'
test_axpy.cc:(.text+0x84): undefined reference to `std::cout'
test_axpy.cc:(.text+0x89): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0xbe): undefined reference to `std::cout'
test_axpy.cc:(.text+0xc3): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0xc8): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::endl<char, std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&)'
test_axpy.cc:(.text+0xd0): undefined reference to `std::ostream::operator<<(std::ostream& (*)(std::ostream&))'
test_axpy.cc:(.text+0xec): undefined reference to `operator delete[](void*)'
test_axpy.cc:(.text+0xff): undefined reference to `operator delete[](void*)'
test_axpy.cc:(.text+0x10e): undefined reference to `__cxa_begin_catch'
test_axpy.cc:(.text+0x118): undefined reference to `std::cout'
test_axpy.cc:(.text+0x11d): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::operator<< <std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&, char const*)'
test_axpy.cc:(.text+0x122): undefined reference to `std::basic_ostream<char, std::char_traits<char> >& std::endl<char, std::char_traits<char> >(std::basic_ostream<char, std::char_traits<char> >&)'
test_axpy.cc:(.text+0x12a): undefined reference to `std::ostream::operator<<(std::ostream& (*)(std::ostream&))'
test_axpy.cc:(.text+0x12f): undefined reference to `__cxa_end_catch'
test_axpy.cc:(.text+0x139): undefined reference to `__cxa_end_catch'
test_axpy.o: In function `__static_initialization_and_destruction_0(int, int)':
test_axpy.cc:(.text+0x172): undefined reference to `std::ios_base::Init::Init()'
test_axpy.cc:(.text+0x181): undefined reference to `std::ios_base::Init::~Init()'
test_axpy.o:(.eh_frame+0x13): undefined reference to `__gxx_personality_v0'
collect2: error: ld returned 1 exit status


I have to use `-fopenmp` when building the object file for `axpy.cc`. 
The program behavior did not change.

13. I guess multiple lines of:

 shasXXXX.rc.int.colorado.edu
 Calling axpy - Passed

will be printed, because the program is now running in parallel threads. 

14. DONE

15. DONE

16. DONE

17. DONE 

