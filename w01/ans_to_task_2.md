4. Done.

5. PATH is changed from `/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin` to `/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64:/curc/sw/gcc/5.4.0/bin:/usr/lpp/mmfs/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/dell/srvadmin/bin`, and LD\_LIBRARY\_PATH is changed from `` to `/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64:/curc/sw/gcc/5.4.0/lib64`.

6. New modules, for example, the one (`impi/17.3`) for MPI Implementations appear. There are also Compiler Dependent Applications that could be further loaded.

7. The hierarchical module system helps prevent loading conflicting dependencies, thus helps development and debugging.

