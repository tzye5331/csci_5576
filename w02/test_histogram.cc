/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 3 $
 * @tags $Tags:  $
 * @date $Date: Tue Sep 26 23:15:22 2017 -0600 $
 *
 * @brief Tester for histogram
 */

#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>

void rand_fill(vec_t &x){

    for (auto &el : x)
        el = 1e-6*(rand() % 1000000); /* between 0 and 1 */
}

int main(int argc, char** argv){

    // repeatable test
    srand(2018);

    // commandline arguments
    if (argc<3){
        printf("Log10 of array size and number of bins are mandatory arugments for %s.\n", __FILE__);
        exit(1);
    }

    size_t n(std::pow(10,atoi(argv[1])));
    size_t num_bins(std::pow(10,atoi(argv[2])));
    double outlier(0);
    if (argc>3) outlier=atof(argv[3]);

    vec_t x(n), bin_bdry;
    count_t bin_count;
    rand_fill(x);
    x[n-1] = outlier;

    int nt = omp_get_max_threads();
    printf("Histogram of a random array of size %d, num_bins=%d, num_threads=%d\n",n,num_bins,nt);
    double tic(NOW());
    histogram(x, num_bins, bin_bdry, bin_count);
    double toc(NOW());
    printf("Elapsed time=%-5.2e\n",toc-tic);

    size_t nn(0);
    for (int iB=0;iB<num_bins;++iB){
        nn += bin_count[iB];
        printf("[bin %d: %5.2e -- %5.2e] %d\n", iB, bin_bdry[iB],bin_bdry[iB+1],bin_count[iB]);
    }
    assert (nn==n);

}
