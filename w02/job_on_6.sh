#!/bin/bash

#SBATCH --time=0:30:00 # walltime, abbreviated by -t
#SBATCH --nodes=1 # number of cluster nodes, abbreviated by -N
#SBATCH -o test_histogram-%j.out # name of the stdout redirection file, using the job number (%j)
#SBATCH -e test_histogram-%j.err # name of the stderr redirection file
#SBATCH --ntasks 1 # number of parallel process
#SBATCH --cpus-per-task=6
#SBATCH --qos normal # quality of service/queue (See QOS section on CU RC guide)

# run the program
echo "Running on $(hostname --fqdn)"
#module load intel

./test_histogram.exe 2 1 >> scaling_a_1E2_1.txt
./test_histogram.exe 2 3 >> scaling_a_1E2_3.txt
./test_histogram.exe 2 4 >> scaling_a_1E2_4.txt

./test_histogram.exe 5 1 >> scaling_a_1E5_1.txt
./test_histogram.exe 5 3 >> scaling_a_1E5_3.txt
./test_histogram.exe 5 4 >> scaling_a_1E5_4.txt

./test_histogram.exe 9 1 >> scaling_a_1E9_1.txt
./test_histogram.exe 9 3 >> scaling_a_1E9_3.txt
./test_histogram.exe 9 4 >> scaling_a_1E9_4.txt

