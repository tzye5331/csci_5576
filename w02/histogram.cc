#ifdef _OPENMP
#include <omp.h>
#define NOW()(omp_get_wtime())
#else
#define NOW() 0
#define omp_get_thread_num() 0
#endif

#include <iostream>
#include "histogram.h"

#define  PRINT_HEADER(msg) printf("\n\nRunning %s\n---\n%s",__FUNCTION__,msg)
#define  TID(msg) printf("Thread id=%d: %s\n",omp_get_thread_num(),msg)

void histogram(const vec_t &x, int num_bins, vec_t& bin_bdry,  count_t& bin_count) {
    double mx = -10000000;
    double mn = 10000000;

    for (unsigned int i = 0; i < x.size(); ++i) {
        if (x[i] > mx) {
            mx = x[i];
        }
        if (x[i] < mn) {
            mn = x[i];
        }
    }
    bin_bdry.resize(num_bins, 0);
    bin_count.resize(num_bins, 0);

    bin_bdry[0] = mn - 0.00001 ;
    bin_bdry[num_bins - 1] = mx + 0.00001;

    double space = (mx - mn) / num_bins;
    for (unsigned int i = 0; i < num_bins; ++i) {
        bin_bdry[i] = mn + space * i;
    }
    

    //#pragma omp parallel 
    //{
        #pragma omp parallel for schedule(static)
        for (unsigned int i = 0;  i < x.size(); ++i) {
            unsigned int j = 0;
            while (x[i] > bin_bdry[j+1]) {
                j += 1;
                if (j == num_bins - 1) {
                    break;
                }
            }
            #pragma omp atomic
            ++bin_count[j];
        }
    //}
}
/*
int main()
{
    size_t n = 10;
    size_t num_bins = 3;
    vec_t x(n), bin_bdry;
    count_t bin_count;
    x[0] = 3;
    x[1] = 7;
    x[2] = 4;
    x[3] = 5;
    x[4] = 1;
    x[5] = 9;
    x[6] = 3;
    x[7] = 2;
    x[8] = 6;
    x[9] = 8;

    histogram(x, num_bins, bin_bdry, bin_count);
    for (int iB=0; iB<num_bins; ++iB) {
        std::cout << "[bin" << iB <<": " << bin_bdry[iB] << " -- " << bin_bdry[iB + 1] << "] " << bin_count[iB] << "\n";
    }

    return 0;
}
*/
