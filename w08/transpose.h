#ifndef _TRANSPOSE_H_
#define _TRANSPOSE_H_

#include <vector>
#include <omp.h>

#define NOW()(omp_get_wtime())


using data_t = double;
using int_vec_t = std::vector<int>;
using double_vec_t = std::vector<double>;


#endif