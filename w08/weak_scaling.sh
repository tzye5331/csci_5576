#!/bin/bash

#SBATCH --time=0:30:00
#SBATCH --nodes=24
#SBATCH -o weak-transpose-%j.out
#SBATCH -e weak-transpose-%j.err
#SBATCH --qos debug

source config.summit.rc

# run the program
for i in {1,6,12,24}; do mpirun -n i ./transpose.exe $((100000*$i)) $((100000*$i)) $((10000000*$i*$i)) > output.dat; done

