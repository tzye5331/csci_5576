Usage:
source config.summit.rc
make clean
make
./transpose.exe <m> <n> <num_values>

Attributes:
<m>: the column dimension of the matrix A
<n>: the row dimension of the matrix A
<num_value>: the number of non-zero entries in the sparse matrix A 

If the code runs without an exception, it performs tha transpose A.transpose() correctly.
