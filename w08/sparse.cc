#include "sparse.h"

void sort_index(const std::vector<size_t> &v, std::vector<size_t> &idx){
    size_t n(0);
    idx.resize(v.size());
    std::generate(idx.begin(), idx.end(), [&]{ return n++; });
    std::sort(idx.begin(), idx.end(),
              [&](size_t ii, size_t jj) { return v[ii] < v[jj]; } );
};

sparse::sparse(size_t m, size_t n, storage_type t) :
        m(m), n(n), type(t) {}
