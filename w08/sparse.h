#ifndef _SPARSE_H_
#define _SPARSE_H_

#include <vector>

typedef double data_t;

/*
 *  Returns the shuffling index set such that v[idx] is sorted
 */
void sort_index(const std::vector <size_t> &v, std::vector <size_t> &idx);

class sparse {
public:
    enum storage_type {
        CSR, COO
    };
    std::vector <data_t> data;
    std::vector <size_t> column;
    std::vector <size_t> row;

    size_t m, n; /* (local) matrix dimensions */
    storage_type type;

    sparse(size_t m, size_t n, storage_type t = CSR);

    void transpose(sparse &B) const; /* returns transpose in B */
};

#endif