#include "transpose.h"
#include <iostream>
#include <mpi.h>
#include <cassert>


int main(int argc, char **argv) {

    assert(argc < 5);
    unsigned int m(atoi(argv[1]));
    unsigned int n(atoi(argv[2]));
    unsigned int n_csr_points{10};
    if (argc > 3) {
        n_csr_points = atoi(argv[3]);
    }

    // initialize CSR values
    int_vec_t col(n_csr_points);
    int_vec_t row(n_csr_points);
    double_vec_t csr_values(n_csr_points);

    // initialize MPI
    MPI_Status status;
    MPI_Init(&argc, &argv);

    // get processor rank
    int rank;
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // assign division arrays
    int_vec_t div{2};

    int_vec_t assign{nproc * 2};

    int_vec_t disp_send{nproc};

    int_vec_t cnt{nproc};

    double tic(NOW());
    // for the root processor (so this only executes once for a parallel run)
    if (rank == 0) {
        for (unsigned int ii = 0; ii < n_csr_points; ii++) {
            col[ii] = rand() % n;
            row[ii] = rand() % m;
            csr_values[ii] = (rand() % (10 * n_csr_points)) / n_csr_points;
        }

        std::cout << "Initialized a sparse matrix of dim (m, n) = (" << m << ", " << n << ") in CSR format with ";
        std::cout << n_csr_points << " random values."  << "\n";

        unsigned int start = 0;
        unsigned int end;
        unsigned int displacement;

        for (unsigned int ii = 0; ii < nproc; ii++) {
            disp_send[ii] = start;
            displacement = ((n_csr_points - (n_csr_points % nproc)) / nproc);
            if (ii < (n_csr_points % nproc)) {
                displacement++;
            }
            end = start + displacement;
            assign[2 * ii] = start;
            assign[2 * ii + 1] = end;
            start = end;
            cnt[ii] = displacement;
        }
    }

    // Assign rows
    MPI_Scatter(&assign[0], 2, MPI_INT, &div[0], 2, MPI_INT, 0, MPI_COMM_WORLD);

    // broadcast csr_values
    MPI_Bcast(&col[0], col.size(), MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&row[0], row.size(), MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&csr_values[0], csr_values.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&cnt[0], cnt.size(), MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&disp_send[0], disp_send.size(), MPI_INT, 0, MPI_COMM_WORLD);

    // transpose matrix based on the assigned division of n_csr_points
    unsigned int jobs = div[1] - div[0];
    int_vec_t col_t(n_csr_points);
    int_vec_t row_t(n_csr_points);

    int_vec_t sub_col_t(jobs);
    int_vec_t sub_row_t(jobs);

    for (unsigned int j = 0; j < jobs; j++) {
        sub_col_t[j] = row[j];
        sub_row_t[j] = col[j];
    }

    // Gather all new csr_values (of varying count of data) together
    MPI_Gatherv(&sub_col_t[0], jobs, MPI_INT, &col_t[0], &cnt[0], &disp_send[0], MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gatherv(&sub_row_t[0], jobs, MPI_INT, &row_t[0], &cnt[0], &disp_send[0], MPI_INT, 0, MPI_COMM_WORLD);


    double toc(NOW());
    if (rank == 0) {
        for (unsigned int r = 0; r < 1; ++r) {
            assert(row[r] == col_t[r] && col[r] == row_t[r]);
        }
    }

    // clean up
    MPI_Finalize();

    assign.clear();
    div.clear();
    disp_send.clear();
    cnt.clear();

    std::cout << "Transpose done. Elapsed time: " << toc - tic << " sec.\n";
    return 0;
}
