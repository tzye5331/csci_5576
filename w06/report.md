1. My code, in general, lacks variables that control the flow of code.

    - Regarding `histogram` - Each thread takes a portion of the input vector, and it will increment its bin count accordingly. Then, local bin counts in each thread will be combined to make the global bin count. 

    - Regarding `quicksort` - When the size of the array is less than 5e4 (NTSK & NWS), `omp parallel` will be used for sorting, but for partition, it will be sequential.
If the size of array is greater than 5e4 (NTSK & NWS), the sorting will be sequential but the partition will be parallel using `omp threads`.
However, when the size of the array is small. say less than 50 (NMIN), the sort and the partition steps will be sequential to avoid unnecessary overhead.

2. Please see `w06/histogram-scaling-report.pdf` and `w06/quicksort-scaling-report.pdf` in this commit. 
 
3. Let's define the length of the input array as `n`. Now, three cases may happen.

    - `n > NWS`: we directly use data-parallel mode, where each thread sort a portion of the data in parallel without using a task pool.
 
    - `n < NTSK`: we put the quicksort tasks into the pool, which will be consumed by the threads.

    - `n < NMIN`: quicksort is called without the task option, due to the overhead of using the threads.

4. The `KMP_AFFINITY` environment variable determines the physical machine topology and assigns OpenMP threads to the
processors based upon their physical location in the machine. This can affect significantly to performance. `Scatter`
(affinity type) is used in this instance to evenly distribute the threads amongst the processors, and `thread` 
(granularity level) is used to bind each OpenMP thread to a single thread context.