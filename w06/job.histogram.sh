#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH -o histogram-%j.out
#SBATCH -e histogram-%j.err
#SBATCH --ntasks 24
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo "weak scaling histogram 2 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 1;done

echo "weak scaling histogram 2 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 3;done

echo "weak scaling histogram 2 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 4;done

echo "strong scaling histogram 5 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 1;done

echo "strong scaling histogram 5 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 3;done

echo "strong scaling histogram 5 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 4;done

echo "strong scaling histogram 9 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 1;done

echo "strong scaling histogram 9 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 3;done

echo "strong scaling histogram 9 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 4;done

echo "with an outlier"

echo "weak scaling histogram 2 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 1 1;done

echo "weak scaling histogram 2 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 3 3;done

echo "weak scaling histogram 2 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 2 4 4;done

echo "strong scaling histogram 5 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 1 1;done

echo "strong scaling histogram 5 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 3 3;done

echo "strong scaling histogram 5 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 5 4 4;done

echo "strong scaling histogram 9 1"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 1 1;done

echo "strong scaling histogram 9 3"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 3 3;done

echo "strong scaling histogram 9 4"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_histogram.exe 9 4 4;done
