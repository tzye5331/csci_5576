#!/bin/bash

#SBATCH --time=1:00:00
#SBATCH --nodes=1
#SBATCH -o quick-sort-%j.out
#SBATCH -e quick-sort-%j.err
#SBATCH --ntasks 24
#SBATCH --qos debug

# run the program
echo "Running on $(hostname --fqdn)"
module load intel

echo "weak scaling quicksort 1M"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 1000000;done

echo "strong scaling quicksort 1M"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((1000000*$i));done

echo "weak scaling quicksort 10M"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 10000000;done

echo "strong scaling quicksort 10M"
for i in {1,2,4,8,12,16,20,24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((10000000*$i));done